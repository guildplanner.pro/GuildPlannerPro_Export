GuildPlannerPro_Export = {}

GuildPlannerPro_Export.Name = "GuildPlannerPro_Export"
GuildPlannerPro_Export.DisplayName = "GuildPlanner.Pro Export"
GuildPlannerPro_Export.AddonVersion = "2.13.1"
GuildPlannerPro_Export.PlayerDataExportSavedVariablesName = "GuildPlannerPro_PlayerDataExport"
GuildPlannerPro_Export.GameDataExportSavedVariablesName = "GuildPlannerPro_GameDataExport"
GuildPlannerPro_Export.VariableVersion = 213010001
GuildPlannerPro_Export.ScanIntervalEveryMinute = 60000

local characterDefault = {
    AvaRank = {},
    Builds = {},
    CarriedSets = {},
    ChampionPoints = {},
    Equipment = {},
    Hotbars = {},
    PveAchievements = {},
    SkillLines = {},
    Stats = {},
}
local accountDefault = {
    ChampionLevel = {},
    BankedSets = {
        [BAG_BANK] = {},
        [BAG_SUBSCRIBER_BANK] = {},
        [BAG_HOUSE_BANK_ONE] = {},
        [BAG_HOUSE_BANK_TWO] = {},
        [BAG_HOUSE_BANK_THREE] = {},
        [BAG_HOUSE_BANK_FOUR] = {},
        [BAG_HOUSE_BANK_FIVE] = {},
        [BAG_HOUSE_BANK_SIX] = {},
        [BAG_HOUSE_BANK_SEVEN] = {},
        [BAG_HOUSE_BANK_EIGHT] = {},
        [BAG_HOUSE_BANK_NINE] = {},
        [BAG_HOUSE_BANK_TEN] = {},
    },
    Guilds = {},
    IsESOPlusSubscriber = IsESOPlusSubscriber(),
    ItemSetCollectionSets = {},
    Language = GetCVar("language.2"),
    PveAchievements = {},
    Stories = {},
    TimeStamp = GetTimeStamp(),
    FormattedTime = GetFormattedTime(),
    SecondsSinceMidnight = GetSecondsSinceMidnight(),
}
local gameDefault = {
    ChampionPoints = {},
    Globals = {},
    ItemSetCollectionSets = {},
    PveAchievements = {},
    SkillLines = {},
    SkillAliases = {},
    Scribables={},
    Stories = {},
}

local character -- SavedVariable for exported character data
local account -- SavedVariable for exported account data
local game -- SavedVariable for exported game data

function GuildPlannerPro_Export:ExportGameData()
    game.PveAchievements = GuildPlannerPro_Achievements:MapPveAchievements{}
    GuildPlannerPro_Utils:PrintMessage("PveAchievements exported.")
    game.ChampionPoints = GuildPlannerPro_Skills:MapChampionPoints()
    GuildPlannerPro_Utils:PrintMessage("Champion Points exported.")
    game.Stories = GuildPlannerPro_Collectibles:MapStoriesCollectibles()
    GuildPlannerPro_Utils:PrintMessage("Stories exported.")
    game.ItemSetCollectionSets = GuildPlannerPro_Sets:MapItemSetCollectionSets()
    GuildPlannerPro_Utils:PrintMessage("ItemSetCollectionSets exported.")
    game.SkillLines = GuildPlannerPro_Skills:MapSkillLines()
    GuildPlannerPro_Utils:PrintMessage("SkillLines exported.")
    game.SkillAliases = GuildPlannerPro_Skills:CollectSkillAliasIds()
    GuildPlannerPro_Utils:PrintMessage("SkillAliases exported.")
    game.Scribables = GuildPlannerPro_Skills:MapScribables()
    GuildPlannerPro_Utils:PrintMessage("Scribables exported.")
    game.Globals = GuildPlannerPro_Const
end

function GuildPlannerPro_Export:ClearGameData()
    game.ChampionPoints = {}
    game.Globals = {}
    game.ItemSetCollectionSets = {}
    game.PveAchievements = {}
    game.SkillLines = {}
    game.Scribables = {}
    game.Stories = {}
end

local function UpdateEveryMinute()
    character.Stats = GuildPlannerPro_Character:ExportCharacterStats()
    account.ItemSetCollectionSets = GuildPlannerPro_Sets:ExportItemSetCollectionSets()
end

local function OnEventAchievementAwarded(_, _, _, achievementId)
    character.AchievementPoints = GetEarnedAchievementPoints()
    local pveAchievements = GuildPlannerPro_Achievements:MapPveAchievements{shallow = true}
    if (GuildPlannerPro_Utils:InTable(achievementId, pveAchievements)) then
        if pveAchievements[achievementId]["PersistenceLevel"] == ACHIEVEMENT_PERSISTENCE_CHARACTER then
            GuildPlannerPro_Achievements:ParseAchievement(achievementId, character.PveAchievements)
        end
        if pveAchievements[achievementId]["PersistenceLevel"] == ACHIEVEMENT_PERSISTENCE_ACCOUNT then
            GuildPlannerPro_Achievements:ParseAchievement(achievementId, account.PveAchievements)
        end
    end
end

local function OnEventActionSlotsAllHotbarsUpdated()
    character.Hotbars = GuildPlannerPro_Skills:ExportCharacterHotBars()
end

local function OnEventArmoryBuildUpdated(_, result, buildIndex)
    if result == ARMORY_BUILD_RESTORE_RESULT_SUCCESS or result == ARMORY_BUILD_SAVE_RESULT_SUCCESS then
        character.Builds[buildIndex] = GuildPlannerPro_Armory:ParseArmoryBuild(buildIndex)
    end
end

local function OnEventChampionPointUpdate(_, unitTag, _, _)
    if unitTag == 'player' then
        account.ChampionLevel = GuildPlannerPro_Character:ExportChampionRank()
    end
end

local function OnEventChampionPurchaseResult(_ , result)
    if result == CHAMPION_PURCHASE_SUCCESS then
        character.ChampionPoints = GuildPlannerPro_Skills:ExportChampionPoints()
        character.Stats = GuildPlannerPro_Character:ExportCharacterStats()
    end
end

local function OnEventCollectionUpdated(_)
    account.Stories = GuildPlannerPro_Collectibles:ExportStoriesCollectibles()
end

local function OnEventGuildUpdate(event, _, arg2, arg3)
    account.Guilds = GuildPlannerPro_Guilds:GetGuilds()
    if event == EVENT_GUILD_ID_CHANGED and account.Guilds[arg2] then -- arg2 = oldGuildId, arg3 = newGuildId
        account.Guilds[arg3]["OldId"] = arg2
    end
end

local function OnEventInventorySingleSlotUpdate(_, bagId)
    if bagId == BAG_WORN then
        character.Equipment = GuildPlannerPro_Character:CheckEquipment()
    elseif bagId == BAG_BACKPACK then
        character.CarriedSets = GuildPlannerPro_Character:CheckBackpackForSets()
    elseif GuildPlannerPro_Utils:TableKeyExists(bagId, account.BankedSets) then
        account.BankedSets[bagId] = GuildPlannerPro_Character:CheckBankForSets(bagId)
        if bagId == BAG_BANK then
            account.BankedSets[BAG_SUBSCRIBER_BANK] = GuildPlannerPro_Character:CheckBankForSets(BAG_SUBSCRIBER_BANK)
        end
    end
end

local function OnEventItemSetCollectionUpdated(_, itemSetId)
    account.ItemSetCollectionSets[itemSetId] = GuildPlannerPro_Sets:ExportItemSetCollectionSet(itemSetId)
end

local function OnEventLevelUpdate()
    character.AvailableSkillPoints = GetAvailableSkillPoints()
    GuildPlannerPro_Character:ExportLevel(character)
end

local function OnEventOpenBank(_, bagId)
    if IsBankOpen() then
        account.BankedSets[bagId] = GuildPlannerPro_Character:CheckBankForSets(bagId)
        if bagId == BAG_BANK then
            account.BankedSets[BAG_SUBSCRIBER_BANK] = GuildPlannerPro_Character:CheckBankForSets(BAG_SUBSCRIBER_BANK)
        end
    end
end

local function OnEventSkillPointsChanged()
    character.AvailableSkillPoints = GetAvailableSkillPoints()
end

local function OnEventTitleUpdate(_, unitTag)
    if unitTag == "player" then
        character.Title = GetUnitTitle("player")
    end
end

local function OnEventTradeSucceeded()
    character.CarriedSets = GuildPlannerPro_Character:CheckBackpackForSets()
end

local function OnEventPlayerActivated()
    GuildPlannerPro_Export:ClearGameData()
end

local function OnEventAddOnLoaded(_, addonName)
    if (addonName ~= GuildPlannerPro_Export.Name) then
        return
    end

    EVENT_MANAGER:UnregisterForEvent(addonName, EVENT_ADD_ON_LOADED)

    -- Register saved variables
    character = ZO_SavedVars:NewCharacterIdSettings(GuildPlannerPro_Export.PlayerDataExportSavedVariablesName, GuildPlannerPro_Export.VariableVersion, nil, characterDefault, GetWorldName())
    account = ZO_SavedVars:NewAccountWide(GuildPlannerPro_Export.PlayerDataExportSavedVariablesName, GuildPlannerPro_Export.VariableVersion, nil, accountDefault, GetWorldName())
    game = ZO_SavedVars:NewAccountWide(GuildPlannerPro_Export.GameDataExportSavedVariablesName, GuildPlannerPro_Export.VariableVersion, nil, gameDefault, GetWorldName())

    ----
    -- Init
    ----
    account.ChampionLevel = GuildPlannerPro_Character:ExportChampionRank()
    account.Guilds = GuildPlannerPro_Guilds:GetGuilds()
    account.IsESOPlusSubscriber = IsESOPlusSubscriber()
    account.ItemSetCollectionSets = GuildPlannerPro_Sets:ExportItemSetCollectionSets()
    account.PveAchievements = GuildPlannerPro_Achievements:ExportAchievementCompletionData(ACHIEVEMENT_PERSISTENCE_ACCOUNT)
    account.Stories = GuildPlannerPro_Collectibles:ExportStoriesCollectibles()
    account.TimeStamp = GetTimeStamp()
    GuildPlannerPro_Character:ExportLevel(character)
    GuildPlannerPro_Character:ExportCharacterBaseInfo(character)
    character.AvaRank = GuildPlannerPro_Character:ExportAvARank()
    character.CarriedSets = GuildPlannerPro_Character:CheckBackpackForSets()
    character.ChampionPoints = GuildPlannerPro_Skills:ExportChampionPoints()
    character.Equipment = GuildPlannerPro_Character:CheckEquipment()
    character.Hotbars = GuildPlannerPro_Skills:ExportCharacterHotBars()
    character.PveAchievements = GuildPlannerPro_Achievements:ExportAchievementCompletionData(ACHIEVEMENT_PERSISTENCE_CHARACTER)
    character.SkillLines = GuildPlannerPro_Skills:ExportCharacterSkillLines()
    character.Stats = GuildPlannerPro_Character:ExportCharacterStats()

    ----
    --  Register Events
    ----
    EVENT_MANAGER:RegisterForEvent(addonName, EVENT_ACHIEVEMENT_AWARDED, OnEventAchievementAwarded)
    EVENT_MANAGER:RegisterForEvent(addonName, EVENT_ACTION_SLOTS_ALL_HOTBARS_UPDATED, OnEventActionSlotsAllHotbarsUpdated)
    EVENT_MANAGER:RegisterForEvent(addonName, EVENT_ARMORY_BUILD_RESTORE_RESPONSE, OnEventArmoryBuildUpdated)
    EVENT_MANAGER:RegisterForEvent(addonName, EVENT_ARMORY_BUILD_SAVE_RESPONSE, OnEventArmoryBuildUpdated)
    EVENT_MANAGER:RegisterForEvent(addonName, EVENT_CHAMPION_POINT_UPDATE, OnEventChampionPointUpdate)
    EVENT_MANAGER:RegisterForEvent(addonName, EVENT_CHAMPION_PURCHASE_RESULT, OnEventChampionPurchaseResult)
    EVENT_MANAGER:RegisterForEvent(addonName, EVENT_COLLECTION_UPDATED, OnEventCollectionUpdated)
    EVENT_MANAGER:RegisterForEvent(addonName, EVENT_GUILD_DESCRIPTION_CHANGED, OnEventGuildUpdate)
    EVENT_MANAGER:RegisterForEvent(addonName, EVENT_GUILD_ID_CHANGED, OnEventGuildUpdate)
    EVENT_MANAGER:RegisterForEvent(addonName, EVENT_GUILD_KEEP_CLAIM_UPDATED, OnEventGuildUpdate)
    EVENT_MANAGER:RegisterForEvent(addonName, EVENT_GUILD_LEVEL_CHANGED, OnEventGuildUpdate)
    EVENT_MANAGER:RegisterForEvent(addonName, EVENT_GUILD_MEMBER_ADDED, OnEventGuildUpdate)
    EVENT_MANAGER:RegisterForEvent(addonName, EVENT_GUILD_MEMBER_DEMOTE_SUCCESSFUL, OnEventGuildUpdate)
    EVENT_MANAGER:RegisterForEvent(addonName, EVENT_GUILD_MEMBER_NOTE_CHANGED, OnEventGuildUpdate)
    EVENT_MANAGER:RegisterForEvent(addonName, EVENT_GUILD_MEMBER_PROMOTE_SUCCESSFUL, OnEventGuildUpdate)
    EVENT_MANAGER:RegisterForEvent(addonName, EVENT_GUILD_MEMBER_RANK_CHANGED, OnEventGuildUpdate)
    EVENT_MANAGER:RegisterForEvent(addonName, EVENT_GUILD_MEMBER_REMOVED, OnEventGuildUpdate)
    EVENT_MANAGER:RegisterForEvent(addonName, EVENT_GUILD_MOTD_CHANGED, OnEventGuildUpdate)
    EVENT_MANAGER:RegisterForEvent(addonName, EVENT_GUILD_PLAYER_RANK_CHANGED, OnEventGuildUpdate)
    EVENT_MANAGER:RegisterForEvent(addonName, EVENT_GUILD_RANKS_CHANGED, OnEventGuildUpdate)
    EVENT_MANAGER:RegisterForEvent(addonName, EVENT_GUILD_RECRUITMENT_INFO_UPDATED, OnEventGuildUpdate)
    EVENT_MANAGER:RegisterForEvent(addonName, EVENT_GUILD_SELF_JOINED_GUILD, OnEventGuildUpdate)
    EVENT_MANAGER:RegisterForEvent(addonName, EVENT_GUILD_SELF_LEFT_GUILD, OnEventGuildUpdate)
    EVENT_MANAGER:RegisterForEvent(addonName, EVENT_GUILD_TRADER_HIRED_UPDATED, OnEventGuildUpdate)
    EVENT_MANAGER:RegisterForEvent(addonName, EVENT_INVENTORY_SINGLE_SLOT_UPDATE, OnEventInventorySingleSlotUpdate)
    EVENT_MANAGER:RegisterForEvent(addonName, EVENT_ITEM_SET_COLLECTION_UPDATED, OnEventItemSetCollectionUpdated)
    EVENT_MANAGER:RegisterForEvent(addonName, EVENT_KEEP_GUILD_CLAIM_UPDATE, OnEventGuildUpdate)
    EVENT_MANAGER:RegisterForEvent(addonName, EVENT_LEVEL_UPDATE, OnEventLevelUpdate)
    EVENT_MANAGER:RegisterForEvent(addonName, EVENT_OPEN_BANK, OnEventOpenBank)
    EVENT_MANAGER:RegisterForEvent(addonName, EVENT_PLAYER_ACTIVATED, OnEventPlayerActivated)
    EVENT_MANAGER:RegisterForEvent(addonName, EVENT_SKILL_POINTS_CHANGED, OnEventSkillPointsChanged)
    EVENT_MANAGER:RegisterForEvent(addonName, EVENT_TITLE_UPDATE, OnEventTitleUpdate)
    EVENT_MANAGER:RegisterForEvent(addonName, EVENT_TRADE_SUCCEEDED, OnEventTradeSucceeded)

    ----
    -- Register Updates
    ----
    EVENT_MANAGER:RegisterForUpdate(addonName, GuildPlannerPro_Export.ScanIntervalEveryMinute, UpdateEveryMinute)
end

----
-- AddOn init
----
EVENT_MANAGER:RegisterForEvent(GuildPlannerPro_Export.Name, EVENT_ADD_ON_LOADED, OnEventAddOnLoaded)
SLASH_COMMANDS["/gppro"] = GuildPlannerPro_Command.Handle
