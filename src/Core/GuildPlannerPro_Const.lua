GuildPlannerPro_Const = {}

GuildPlannerPro_Const.AchievementPersistenceLevel = {}
GuildPlannerPro_Const.AchievementPersistenceLevel[ACHIEVEMENT_PERSISTENCE_ACCOUNT] = "account"
GuildPlannerPro_Const.AchievementPersistenceLevel[ACHIEVEMENT_PERSISTENCE_CHARACTER] = "character"
GuildPlannerPro_Const.AchievementPersistenceLevel[ACHIEVEMENT_PERSISTENCE_UNDEFINED] = "undefined"

GuildPlannerPro_Const.AdvancedStatDisplayType = {}
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_ALLIANCE_POINTS_BONUS] = "alliance_points_bonus"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_ALL_XP] = "all_xp"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_ARMOR] = "armor"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_BASH_COST] = "bash_cost"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_BASH_DAMAGE] = "bash_damage"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_BLEED_DAMAGE] = "bleed_damage"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_BLEED_RESIST] = "bleed_resist"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_BLOCK_COST] = "block_cost"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_BLOCK_MITIGATION] = "block_mitigation"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_BLOCK_SPEED] = "block_speed"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_CC_BREAK_COST] = "cc_break_cost"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_COIN_BONUS] = "coin_bonus"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_COLD_DAMAGE] = "cold_damage"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_COLD_RESIST] = "cold_resist"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_CRITICAL_CHANCE] = "critical_chance"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_CRITICAL_DAMAGE] = "critical_damage"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_CRITICAL_HEALING] = "critical_healing"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_CRITICAL_PERCENT] = "critical_percent"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_CRITICAL_RESIST] = "critical_resist"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_DISEASE_DAMAGE] = "disease_damage"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_DISEASE_RESIST] = "disease_resist"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_DODGE_COST] = "dodge_cost"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_EARTH_DAMAGE] = "earth_damage"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_EARTH_RESIST] = "earth_resist"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_FIRE_DAMAGE] = "fire_damage"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_FIRE_RESIST] = "fire_resist"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_FROST_RESIST] = "frost_resist"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_GENERIC_DAMAGE] = "generic_damage"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_GENERIC_RESIST] = "generic_resist"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_HEALING_DONE] = "healing_done"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_HEALING_DONE_BONUSES] = "healing_done_bonuses"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_HEALING_TAKEN] = "healing_taken"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_HEALING_TAKEN_BONUSES] = "healing_taken_bonuses"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_INSPIRATION_BONUS] = "inspiration_bonus"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_MAGIC_DAMAGE] = "magic_damage"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_MAGIC_RESIST] = "magic_resist"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_MONSTER_KILL_XP] = "monster_kill_xp"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_NONE] = "none"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_OBLIVION_DAMAGE] = "oblivion_damage"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_OBLIVION_RESIST] = "oblivion_resist"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_PHYSICAL_DAMAGE] = "physical_damage"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_PHYSICAL_PENETRATION] = "physical_penetration"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_PHYSICAL_RESIST] = "physical_resist"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_PLAYER_KILL_XP] = "player_kill_xp"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_POISON_DAMAGE] = "poison_damage"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_POISON_RESIST] = "poison_resist"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_SHOCK_DAMAGE] = "shock_damage"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_SHOCK_RESIST] = "shock_resist"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_SNEAK_COST] = "sneak_cost"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_SNEAK_SPEED_REDUCTION] = "sneak_speed_reduction"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_SPELL_PENETRATION] = "spell_penetration"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_SPELL_RESIST] = "spell_resist"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_SPRINT_COST] = "sprint_cost"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_SPRINT_SPEED] = "sprint_speed"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_SUBSCRIBER_ALLIANCE_POINTS_BONUS] = "subscriber_alliance_points_bonus"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_SUBSCRIBER_ALL_XP] = "subscriber_all_xp"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_SUBSCRIBER_COIN_BONUS] = "subscriber_coin_bonus"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_SUBSCRIBER_INSPIRATION_BONUS] = "subscriber_inspiration_bonus"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_SUBSCRIBER_TELVAR_BONUS] = "subscriber_telvar_bonus"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_TELVAR_BONUS] = "telvar_bonus"
GuildPlannerPro_Const.AdvancedStatDisplayType[ADVANCED_STAT_DISPLAY_TYPE_ULTIMATE_REGEN_COMBAT] = "ultimate_regen_combat"

GuildPlannerPro_Const.Alliance = {}
GuildPlannerPro_Const.Alliance[ALLIANCE_ALDMERI_DOMINION] = "aldmeri_dominion"
GuildPlannerPro_Const.Alliance[ALLIANCE_DAGGERFALL_COVENANT] = "daggerfall_covenant"
GuildPlannerPro_Const.Alliance[ALLIANCE_EBONHEART_PACT] = "ebonheart_pact"
GuildPlannerPro_Const.Alliance[ALLIANCE_NONE] = "none"

GuildPlannerPro_Const.ArmorTypes = {}
GuildPlannerPro_Const.ArmorTypes[ARMORTYPE_HEAVY] = "heavy"
GuildPlannerPro_Const.ArmorTypes[ARMORTYPE_LIGHT] = "light"
GuildPlannerPro_Const.ArmorTypes[ARMORTYPE_MEDIUM] = "medium"
GuildPlannerPro_Const.ArmorTypes[ARMORTYPE_NONE] = "none"

GuildPlannerPro_Const.ArmoryBuildEquipSlotState = {}
GuildPlannerPro_Const.ArmoryBuildEquipSlotState[ARMORY_BUILD_EQUIP_SLOT_STATE_EMPTY] = "empty"
GuildPlannerPro_Const.ArmoryBuildEquipSlotState[ARMORY_BUILD_EQUIP_SLOT_STATE_INACCESSIBLE] = "inaccessible"
GuildPlannerPro_Const.ArmoryBuildEquipSlotState[ARMORY_BUILD_EQUIP_SLOT_STATE_MISSING] = "missing"
GuildPlannerPro_Const.ArmoryBuildEquipSlotState[ARMORY_BUILD_EQUIP_SLOT_STATE_VALID] = "valid"

GuildPlannerPro_Const.Bag = {}
GuildPlannerPro_Const.Bag[BAG_BACKPACK] = "backpack"
GuildPlannerPro_Const.Bag[BAG_BANK] = "bank"
GuildPlannerPro_Const.Bag[BAG_BUYBACK] = "buyback"
GuildPlannerPro_Const.Bag[BAG_COMPANION_WORN] = "companion_worn"
GuildPlannerPro_Const.Bag[BAG_GUILDBANK] = "guildbank"
GuildPlannerPro_Const.Bag[BAG_HOUSE_BANK_EIGHT] = "house_bank_eight"
GuildPlannerPro_Const.Bag[BAG_HOUSE_BANK_FIVE] = "house_bank_five"
GuildPlannerPro_Const.Bag[BAG_HOUSE_BANK_FOUR] = "house_bank_four"
GuildPlannerPro_Const.Bag[BAG_HOUSE_BANK_NINE] = "house_bank_nine"
GuildPlannerPro_Const.Bag[BAG_HOUSE_BANK_ONE] = "house_bank_one"
GuildPlannerPro_Const.Bag[BAG_HOUSE_BANK_SEVEN] = "house_bank_seven"
GuildPlannerPro_Const.Bag[BAG_HOUSE_BANK_SIX] = "house_bank_six"
GuildPlannerPro_Const.Bag[BAG_HOUSE_BANK_TEN] = "house_bank_ten"
GuildPlannerPro_Const.Bag[BAG_HOUSE_BANK_THREE] = "house_bank_three"
GuildPlannerPro_Const.Bag[BAG_HOUSE_BANK_TWO] = "house_bank_two"
GuildPlannerPro_Const.Bag[BAG_SUBSCRIBER_BANK] = "subscriber_bank"
GuildPlannerPro_Const.Bag[BAG_VIRTUAL] = "virtual"
GuildPlannerPro_Const.Bag[BAG_WORN] = "worn"

GuildPlannerPro_Const.BindType = {}
GuildPlannerPro_Const.BindType[BIND_TYPE_NONE] = "none"
GuildPlannerPro_Const.BindType[BIND_TYPE_ON_EQUIP] = "on_equip"
GuildPlannerPro_Const.BindType[BIND_TYPE_ON_PICKUP] = "on_pickup"
GuildPlannerPro_Const.BindType[BIND_TYPE_ON_PICKUP_BACKPACK] = "on_pickup_backpack"
GuildPlannerPro_Const.BindType[BIND_TYPE_UNSET] = "unset"

GuildPlannerPro_Const.BuffType = {}
GuildPlannerPro_Const.BuffType[BUFF_TYPE_EMPOWER] = "EMPOWER"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MAJOR_AEGIS] = "MAJOR AEGIS"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MAJOR_BERSERK] = "MAJOR BERSERK"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MAJOR_BREACH] = "MAJOR BREACH"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MAJOR_BRITTLE] = "MAJOR BRITTLE"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MAJOR_BRUTALITY] = "MAJOR BRUTALITY"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MAJOR_COURAGE] = "MAJOR COURAGE"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MAJOR_COWARDICE] = "MAJOR COWARDICE"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MAJOR_DEFILE] = "MAJOR DEFILE"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MAJOR_ENDURANCE] = "MAJOR ENDURANCE"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MAJOR_EVASION] = "MAJOR EVASION"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MAJOR_EXPEDITION] = "MAJOR EXPEDITION"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MAJOR_FORCE] = "MAJOR FORCE"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MAJOR_FORTITUDE] = "MAJOR FORTITUDE"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MAJOR_GALLOP] = "MAJOR GALLOP"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MAJOR_HEROISM] = "MAJOR HEROISM"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MAJOR_INTELLECT] = "MAJOR INTELLECT"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MAJOR_MAIM] = "MAJOR MAIM"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MAJOR_MANGLE] = "MAJOR MANGLE"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MAJOR_MENDING] = "MAJOR MENDING"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MAJOR_PROPHECY] = "MAJOR PROPHECY"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MAJOR_PROTECTION] = "MAJOR PROTECTION"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MAJOR_RESOLVE] = "MAJOR RESOLVE"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MAJOR_SAVAGERY] = "MAJOR SAVAGERY"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MAJOR_SLAYER] = "MAJOR SLAYER"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MAJOR_SORCERY] = "MAJOR SORCERY"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MAJOR_TIMIDITY] = "MAJOR TIMIDITY"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MAJOR_VITALITY] = "MAJOR VITALITY"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MAJOR_VULNERABILITY] = "MAJOR VULNERABILITY"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MINOR_AEGIS] = "MINOR AEGIS"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MINOR_BERSERK] = "MINOR BERSERK"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MINOR_BREACH] = "MINOR BREACH"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MINOR_BRITTLE] = "MINOR BRITTLE"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MINOR_BRUTALITY] = "MINOR BRUTALITY"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MINOR_COURAGE] = "MINOR COURAGE"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MINOR_COWARDICE] = "MINOR COWARDICE"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MINOR_DEFILE] = "MINOR DEFILE"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MINOR_ENDURANCE] = "MINOR ENDURANCE"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MINOR_ENERVATION] = "MINOR ENERVATION"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MINOR_EVASION] = "MINOR EVASION"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MINOR_EXPEDITION] = "MINOR EXPEDITION"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MINOR_FORCE] = "MINOR FORCE"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MINOR_FORTITUDE] = "MINOR FORTITUDE"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MINOR_GALLOP] = "MINOR GALLOP"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MINOR_HEROISM] = "MINOR HEROISM"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MINOR_INTELLECT] = "MINOR INTELLECT"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MINOR_LIFESTEAL] = "MINOR LIFESTEAL"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MINOR_MAGICKASTEAL] = "MINOR MAGICKASTEAL"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MINOR_MAIM] = "MINOR MAIM"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MINOR_MANGLE] = "MINOR MANGLE"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MINOR_MENDING] = "MINOR MENDING"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MINOR_PROPHECY] = "MINOR PROPHECY"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MINOR_PROTECTION] = "MINOR PROTECTION"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MINOR_RESOLVE] = "MINOR RESOLVE"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MINOR_SAVAGERY] = "MINOR SAVAGERY"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MINOR_SLAYER] = "MINOR SLAYER"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MINOR_SORCERY] = "MINOR SORCERY"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MINOR_TIMIDITY] = "MINOR TIMIDITY"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MINOR_TOUGHNESS] = "MINOR TOUGHNESS"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MINOR_UNCERTAINTY] = "MINOR UNCERTAINTY"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MINOR_VITALITY] = "MINOR VITALITY"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_MINOR_VULNERABILITY] = "MINOR VULNERABILITY"
GuildPlannerPro_Const.BuffType[BUFF_TYPE_NONE] = "NONE"

GuildPlannerPro_Const.ChampionDisciplineType = {}
GuildPlannerPro_Const.ChampionDisciplineType[CHAMPION_DISCIPLINE_TYPE_COMBAT] = "combat"
GuildPlannerPro_Const.ChampionDisciplineType[CHAMPION_DISCIPLINE_TYPE_CONDITIONING] = "conditioning"
GuildPlannerPro_Const.ChampionDisciplineType[CHAMPION_DISCIPLINE_TYPE_WORLD] = "world"

GuildPlannerPro_Const.ChampionSkillType = {}
GuildPlannerPro_Const.ChampionSkillType[CHAMPION_SKILL_TYPE_NORMAL] = "normal"
GuildPlannerPro_Const.ChampionSkillType[CHAMPION_SKILL_TYPE_NORMAL_SLOTTABLE] = "normal_slottable"
GuildPlannerPro_Const.ChampionSkillType[CHAMPION_SKILL_TYPE_STAT_POOL_SLOTTABLE] = "stat_pool_slottable"

GuildPlannerPro_Const.Classes = {}
local numClasses = GetNumClasses()
if numClasses > 0 then
    for classIndex = 1, numClasses do
        local classId, lore, normalIconKeyboard, pressedIconKeyboard, mouseoverIconKeyboard, isSelectable, ingameIconKeyboard = GetClassInfo(classIndex)
        GuildPlannerPro_Const.Classes[classId] = {
            Id = classId,
            Index = GetClassIndexById(classId),
            Name = GetClassName(GENDER_MALE, classId),
            Lore = lore,
            NormalIconKeyboard = normalIconKeyboard,
            PressedIconKeyboard = pressedIconKeyboard,
            MouseOverIconKeyboard = mouseoverIconKeyboard,
            IngameIconKeyboard = ingameIconKeyboard,
            IsSelectable = isSelectable,
        }
    end
end

GuildPlannerPro_Const.CombatMechanicFlags = {}
GuildPlannerPro_Const.CombatMechanicFlags[COMBAT_MECHANIC_FLAGS_DAEDRIC] = "daedric"
GuildPlannerPro_Const.CombatMechanicFlags[COMBAT_MECHANIC_FLAGS_HEALTH] = "health"
GuildPlannerPro_Const.CombatMechanicFlags[COMBAT_MECHANIC_FLAGS_MAGICKA] = "magicka"
GuildPlannerPro_Const.CombatMechanicFlags[COMBAT_MECHANIC_FLAGS_MOUNT_STAMINA] = "mount_stamina"
GuildPlannerPro_Const.CombatMechanicFlags[COMBAT_MECHANIC_FLAGS_STAMINA] = "stamina"
GuildPlannerPro_Const.CombatMechanicFlags[COMBAT_MECHANIC_FLAGS_ULTIMATE] = "ultimate"
GuildPlannerPro_Const.CombatMechanicFlags[COMBAT_MECHANIC_FLAGS_WEREWOLF] = "werewolf"

GuildPlannerPro_Const.CurrencyType = {}
GuildPlannerPro_Const.CurrencyType[CURT_ALLIANCE_POINTS] = "alliance_points"
GuildPlannerPro_Const.CurrencyType[CURT_ARCHIVAL_FORTUNES] = "archival_fortunes"
GuildPlannerPro_Const.CurrencyType[CURT_CHAOTIC_CREATIA] = "chaotic_creatia"
GuildPlannerPro_Const.CurrencyType[CURT_CROWNS] = "crowns"
GuildPlannerPro_Const.CurrencyType[CURT_CROWN_GEMS] = "crown_gems"
GuildPlannerPro_Const.CurrencyType[CURT_ENDEAVOR_SEALS] = "endeavor_seals"
GuildPlannerPro_Const.CurrencyType[CURT_EVENT_TICKETS] = "event_tickets"
GuildPlannerPro_Const.CurrencyType[CURT_IMPERIAL_FRAGMENTS] = "imperial_fragments"
GuildPlannerPro_Const.CurrencyType[CURT_MONEY] = "money"
GuildPlannerPro_Const.CurrencyType[CURT_NONE] = "none"
GuildPlannerPro_Const.CurrencyType[CURT_STYLE_STONES] = "style_stones"
GuildPlannerPro_Const.CurrencyType[CURT_TELVAR_STONES] = "telvar_stones"
GuildPlannerPro_Const.CurrencyType[CURT_UNDAUNTED_KEYS] = "undaunted_keys"
GuildPlannerPro_Const.CurrencyType[CURT_WRIT_VOUCHERS] = "writ_vouchers"

GuildPlannerPro_Const.DerivedStats = {}
GuildPlannerPro_Const.DerivedStats[STAT_ARMOR_RATING] = "armor_rating"
GuildPlannerPro_Const.DerivedStats[STAT_ATTACK_POWER] = "attack_power"
GuildPlannerPro_Const.DerivedStats[STAT_BLOCK] = "block"
GuildPlannerPro_Const.DerivedStats[STAT_CRITICAL_CHANCE] = "critical_chance"
GuildPlannerPro_Const.DerivedStats[STAT_CRITICAL_RESISTANCE] = "critical_resistance"
GuildPlannerPro_Const.DerivedStats[STAT_CRITICAL_STRIKE] = "critical_strike"
GuildPlannerPro_Const.DerivedStats[STAT_DAMAGE_RESIST_BLEED] = "damage_resist_bleed"
GuildPlannerPro_Const.DerivedStats[STAT_DAMAGE_RESIST_COLD] = "damage_resist_cold"
GuildPlannerPro_Const.DerivedStats[STAT_DAMAGE_RESIST_DISEASE] = "damage_resist_disease"
GuildPlannerPro_Const.DerivedStats[STAT_DAMAGE_RESIST_DROWN] = "damage_resist_drown"
GuildPlannerPro_Const.DerivedStats[STAT_DAMAGE_RESIST_EARTH] = "damage_resist_earth"
GuildPlannerPro_Const.DerivedStats[STAT_DAMAGE_RESIST_FIRE] = "damage_resist_fire"
GuildPlannerPro_Const.DerivedStats[STAT_DAMAGE_RESIST_GENERIC] = "damage_resist_generic"
GuildPlannerPro_Const.DerivedStats[STAT_DAMAGE_RESIST_MAGIC] = "damage_resist_magic"
GuildPlannerPro_Const.DerivedStats[STAT_DAMAGE_RESIST_OBLIVION] = "damage_resist_oblivion"
GuildPlannerPro_Const.DerivedStats[STAT_DAMAGE_RESIST_PHYSICAL] = "damage_resist_physical"
GuildPlannerPro_Const.DerivedStats[STAT_DAMAGE_RESIST_POISON] = "damage_resist_poison"
GuildPlannerPro_Const.DerivedStats[STAT_DAMAGE_RESIST_SHOCK] = "damage_resist_shock"
GuildPlannerPro_Const.DerivedStats[STAT_DAMAGE_RESIST_START] = "damage_resist_start"
GuildPlannerPro_Const.DerivedStats[STAT_DODGE] = "dodge"
GuildPlannerPro_Const.DerivedStats[STAT_HEALING_DONE] = "healing_done"
GuildPlannerPro_Const.DerivedStats[STAT_HEALING_TAKEN] = "healing_taken"
GuildPlannerPro_Const.DerivedStats[STAT_HEALTH_MAX] = "health_max"
GuildPlannerPro_Const.DerivedStats[STAT_HEALTH_REGEN_COMBAT] = "health_regen_combat"
GuildPlannerPro_Const.DerivedStats[STAT_HEALTH_REGEN_IDLE] = "health_regen_idle"
GuildPlannerPro_Const.DerivedStats[STAT_MAGICKA_MAX] = "magicka_max"
GuildPlannerPro_Const.DerivedStats[STAT_MAGICKA_REGEN_COMBAT] = "magicka_regen_combat"
GuildPlannerPro_Const.DerivedStats[STAT_MAGICKA_REGEN_IDLE] = "magicka_regen_idle"
GuildPlannerPro_Const.DerivedStats[STAT_MISS] = "miss"
GuildPlannerPro_Const.DerivedStats[STAT_MITIGATION] = "mitigation"
GuildPlannerPro_Const.DerivedStats[STAT_MOUNT_STAMINA_MAX] = "mount_stamina_max"
GuildPlannerPro_Const.DerivedStats[STAT_MOUNT_STAMINA_REGEN_COMBAT] = "mount_stamina_regen_combat"
GuildPlannerPro_Const.DerivedStats[STAT_MOUNT_STAMINA_REGEN_MOVING] = "mount_stamina_regen_moving"
GuildPlannerPro_Const.DerivedStats[STAT_NONE] = "none"
GuildPlannerPro_Const.DerivedStats[STAT_OFFENSIVE_PENETRATION] = "offensive_penetration"
GuildPlannerPro_Const.DerivedStats[STAT_PHYSICAL_PENETRATION] = "physical_penetration"
GuildPlannerPro_Const.DerivedStats[STAT_PHYSICAL_RESIST] = "physical_resist"
GuildPlannerPro_Const.DerivedStats[STAT_POWER] = "power"
GuildPlannerPro_Const.DerivedStats[STAT_SPELL_CRITICAL] = "spell_critical"
GuildPlannerPro_Const.DerivedStats[STAT_SPELL_MITIGATION] = "spell_mitigation"
GuildPlannerPro_Const.DerivedStats[STAT_SPELL_PENETRATION] = "spell_penetration"
GuildPlannerPro_Const.DerivedStats[STAT_SPELL_POWER] = "spell_power"
GuildPlannerPro_Const.DerivedStats[STAT_SPELL_RESIST] = "spell_resist"
GuildPlannerPro_Const.DerivedStats[STAT_STAMINA_MAX] = "stamina_max"
GuildPlannerPro_Const.DerivedStats[STAT_STAMINA_REGEN_COMBAT] = "stamina_regen_combat"
GuildPlannerPro_Const.DerivedStats[STAT_STAMINA_REGEN_IDLE] = "stamina_regen_idle"
GuildPlannerPro_Const.DerivedStats[STAT_WEAPON_AND_SPELL_DAMAGE] = "weapon_and_spell_damage"

GuildPlannerPro_Const.DyeHueCategory = {}
GuildPlannerPro_Const.DyeHueCategory[DYE_HUE_CATEGORY_BLUE] = "blue"
GuildPlannerPro_Const.DyeHueCategory[DYE_HUE_CATEGORY_BROWN] = "brown"
GuildPlannerPro_Const.DyeHueCategory[DYE_HUE_CATEGORY_GREEN] = "green"
GuildPlannerPro_Const.DyeHueCategory[DYE_HUE_CATEGORY_GREY] = "grey"
GuildPlannerPro_Const.DyeHueCategory[DYE_HUE_CATEGORY_IRIDESCENT] = "iridescent"
GuildPlannerPro_Const.DyeHueCategory[DYE_HUE_CATEGORY_MIXED] = "mixed"
GuildPlannerPro_Const.DyeHueCategory[DYE_HUE_CATEGORY_PURPLE] = "purple"
GuildPlannerPro_Const.DyeHueCategory[DYE_HUE_CATEGORY_RED] = "red"
GuildPlannerPro_Const.DyeHueCategory[DYE_HUE_CATEGORY_YELLOW] = "yellow"

GuildPlannerPro_Const.DyeRarity = {}
GuildPlannerPro_Const.DyeRarity[DYE_RARITY_COMMON] = "common"
GuildPlannerPro_Const.DyeRarity[DYE_RARITY_MATERIAL] = "material"
GuildPlannerPro_Const.DyeRarity[DYE_RARITY_RARE] = "rare"
GuildPlannerPro_Const.DyeRarity[DYE_RARITY_UNCOMMON] = "uncommon"

GuildPlannerPro_Const.Gender = {}
GuildPlannerPro_Const.Gender[GENDER_FEMALE] = "female"
GuildPlannerPro_Const.Gender[GENDER_MALE] = "male"
GuildPlannerPro_Const.Gender[GENDER_NEUTER] = "neuter"

GuildPlannerPro_Const.GuildRecruitmentStatusAttributeValue = {}
GuildPlannerPro_Const.GuildRecruitmentStatusAttributeValue[GUILD_RECRUITMENT_STATUS_ATTRIBUTE_VALUE_LISTED] = "listed"
GuildPlannerPro_Const.GuildRecruitmentStatusAttributeValue[GUILD_RECRUITMENT_STATUS_ATTRIBUTE_VALUE_NOT_LISTED] = "not_listed"

GuildPlannerPro_Const.GuildFocusAttributeValue = {}
GuildPlannerPro_Const.GuildFocusAttributeValue[GUILD_FOCUS_ATTRIBUTE_VALUE_CRAFTING] = "crafting"
GuildPlannerPro_Const.GuildFocusAttributeValue[GUILD_FOCUS_ATTRIBUTE_VALUE_GROUP_PVE] = "group_pve"
GuildPlannerPro_Const.GuildFocusAttributeValue[GUILD_FOCUS_ATTRIBUTE_VALUE_NONE] = "none"
GuildPlannerPro_Const.GuildFocusAttributeValue[GUILD_FOCUS_ATTRIBUTE_VALUE_PVP] = "pvp"
GuildPlannerPro_Const.GuildFocusAttributeValue[GUILD_FOCUS_ATTRIBUTE_VALUE_QUESTING] = "questing"
GuildPlannerPro_Const.GuildFocusAttributeValue[GUILD_FOCUS_ATTRIBUTE_VALUE_ROLEPLAYING] = "roleplaying"
GuildPlannerPro_Const.GuildFocusAttributeValue[GUILD_FOCUS_ATTRIBUTE_VALUE_SOCIAL] = "social"
GuildPlannerPro_Const.GuildFocusAttributeValue[GUILD_FOCUS_ATTRIBUTE_VALUE_TRADING] = "trading"

GuildPlannerPro_Const.GuildPersonalityAttributeValue = {}
GuildPlannerPro_Const.GuildPersonalityAttributeValue[GUILD_PERSONALITY_ATTRIBUTE_VALUE_BALANCED] = "balanced"
GuildPlannerPro_Const.GuildPersonalityAttributeValue[GUILD_PERSONALITY_ATTRIBUTE_VALUE_CASUAL] = "casual"
GuildPlannerPro_Const.GuildPersonalityAttributeValue[GUILD_PERSONALITY_ATTRIBUTE_VALUE_HARDCORE] = "hardcore"
GuildPlannerPro_Const.GuildPersonalityAttributeValue[GUILD_PERSONALITY_ATTRIBUTE_VALUE_NONE] = "none"

GuildPlannerPro_Const.GuildPermission = {}
GuildPlannerPro_Const.GuildPermission[GUILD_PERMISSION_BANK_DEPOSIT] = "bank_deposit"
GuildPlannerPro_Const.GuildPermission[GUILD_PERMISSION_BANK_VIEW_DEPOSIT_HISTORY] = "bank_view_deposit_history"
GuildPlannerPro_Const.GuildPermission[GUILD_PERMISSION_BANK_VIEW_GOLD] = "bank_view_gold"
GuildPlannerPro_Const.GuildPermission[GUILD_PERMISSION_BANK_VIEW_WITHDRAW_HISTORY] = "bank_view_withdraw_history"
GuildPlannerPro_Const.GuildPermission[GUILD_PERMISSION_BANK_WITHDRAW] = "bank_withdraw"
GuildPlannerPro_Const.GuildPermission[GUILD_PERMISSION_BANK_WITHDRAW_GOLD] = "bank_withdraw_gold"
GuildPlannerPro_Const.GuildPermission[GUILD_PERMISSION_CHAT] = "chat"
GuildPlannerPro_Const.GuildPermission[GUILD_PERMISSION_CLAIM_AVA_RESOURCE] = "claim_ava_resource"
GuildPlannerPro_Const.GuildPermission[GUILD_PERMISSION_DEMOTE] = "demote"
GuildPlannerPro_Const.GuildPermission[GUILD_PERMISSION_DESCRIPTION_EDIT] = "description_edit"
GuildPlannerPro_Const.GuildPermission[GUILD_PERMISSION_EDIT_HERALDRY] = "edit_heraldry"
GuildPlannerPro_Const.GuildPermission[GUILD_PERMISSION_GUILD_KIOSK_BID] = "guild_kiosk_bid"
GuildPlannerPro_Const.GuildPermission[GUILD_PERMISSION_INVITE] = "invite"
GuildPlannerPro_Const.GuildPermission[GUILD_PERMISSION_MANAGE_APPLICATIONS] = "manage_applications"
GuildPlannerPro_Const.GuildPermission[GUILD_PERMISSION_MANAGE_BLACKLIST] = "manage_blacklist"
GuildPlannerPro_Const.GuildPermission[GUILD_PERMISSION_NONE] = "none"
GuildPlannerPro_Const.GuildPermission[GUILD_PERMISSION_NOTE_EDIT] = "note_edit"
GuildPlannerPro_Const.GuildPermission[GUILD_PERMISSION_NOTE_READ] = "note_read"
GuildPlannerPro_Const.GuildPermission[GUILD_PERMISSION_OFFICER_CHAT_READ] = "officer_chat_read"
GuildPlannerPro_Const.GuildPermission[GUILD_PERMISSION_OFFICER_CHAT_WRITE] = "officer_chat_write"
GuildPlannerPro_Const.GuildPermission[GUILD_PERMISSION_PERMISSION_EDIT] = "permission_edit"
GuildPlannerPro_Const.GuildPermission[GUILD_PERMISSION_PROMOTE] = "promote"
GuildPlannerPro_Const.GuildPermission[GUILD_PERMISSION_RECRUITMENT_EDIT] = "recruitment_edit"
GuildPlannerPro_Const.GuildPermission[GUILD_PERMISSION_RELEASE_AVA_RESOURCE] = "release_ava_resource"
GuildPlannerPro_Const.GuildPermission[GUILD_PERMISSION_REMOVE] = "remove"
GuildPlannerPro_Const.GuildPermission[GUILD_PERMISSION_SET_MOTD] = "set_motd"
GuildPlannerPro_Const.GuildPermission[GUILD_PERMISSION_STORE_BUY] = "store_buy"
GuildPlannerPro_Const.GuildPermission[GUILD_PERMISSION_STORE_SELL] = "store_sell"

GuildPlannerPro_Const.GuildPermissionDependencies = {}
for permission = GUILD_PERMISSION_ITERATION_BEGIN, GUILD_PERMISSION_ITERATION_END do
    numDependencies = GetNumGuildPermissionDependencies(permission)
    if numDependencies > 0 then
        GuildPlannerPro_Const.GuildPermissionDependencies[permission] = {}
        for dependency = 1, numDependencies do
            GuildPlannerPro_Const.GuildPermissionDependencies[permission][dependency] = GetGuildPermissionDependency(permission, dependency)
        end
    end
end

GuildPlannerPro_Const.GuildPermissionRequisites = {}
for permission = GUILD_PERMISSION_ITERATION_BEGIN, GUILD_PERMISSION_ITERATION_END do
    numRequisites = GetNumGuildPermissionRequisites(permission)
    if numRequisites > 0 then
        GuildPlannerPro_Const.GuildPermissionRequisites[permission] = {}
        for requisite = 1, numRequisites do
            GuildPlannerPro_Const.GuildPermissionRequisites[permission][requisite] = GetGuildPermissionRequisite(permission, requisite)
        end
    end
end

GuildPlannerPro_Const.GuildPrivilege = {}
GuildPlannerPro_Const.GuildPrivilege[GUILD_PRIVILEGE_BANK_DEPOSIT] = "bank_deposit"
GuildPlannerPro_Const.GuildPrivilege[GUILD_PRIVILEGE_HERALDRY] = "heraldry"
GuildPlannerPro_Const.GuildPrivilege[GUILD_PRIVILEGE_TRADING_HOUSE] = "trading_house"

GuildPlannerPro_Const.GuildPrivilegesMemberNumberRequirements = {
    [GuildPlannerPro_Const.GuildPrivilege[GUILD_PRIVILEGE_BANK_DEPOSIT]] = GetNumGuildMembersRequiredForPrivilege(GUILD_PRIVILEGE_BANK_DEPOSIT),
    [GuildPlannerPro_Const.GuildPrivilege[GUILD_PRIVILEGE_HERALDRY]] = GetNumGuildMembersRequiredForPrivilege(GUILD_PRIVILEGE_HERALDRY),
    [GuildPlannerPro_Const.GuildPrivilege[GUILD_PRIVILEGE_TRADING_HOUSE]] = GetNumGuildMembersRequiredForPrivilege(GUILD_PRIVILEGE_TRADING_HOUSE),
}

GuildPlannerPro_Const.GuildLanguageAttributeValue = {}
GuildPlannerPro_Const.GuildLanguageAttributeValue[GUILD_LANGUAGE_ATTRIBUTE_VALUE_ENGLISH] = "english"
GuildPlannerPro_Const.GuildLanguageAttributeValue[GUILD_LANGUAGE_ATTRIBUTE_VALUE_FRENCH] = "french"
GuildPlannerPro_Const.GuildLanguageAttributeValue[GUILD_LANGUAGE_ATTRIBUTE_VALUE_GERMAN] = "german"
GuildPlannerPro_Const.GuildLanguageAttributeValue[GUILD_LANGUAGE_ATTRIBUTE_VALUE_JAPANESE] = "japanese"
GuildPlannerPro_Const.GuildLanguageAttributeValue[GUILD_LANGUAGE_ATTRIBUTE_VALUE_NONE] = "none"
GuildPlannerPro_Const.GuildLanguageAttributeValue[GUILD_LANGUAGE_ATTRIBUTE_VALUE_OTHER] = "other"
GuildPlannerPro_Const.GuildLanguageAttributeValue[GUILD_LANGUAGE_ATTRIBUTE_VALUE_RUSSIAN] = "russian"
GuildPlannerPro_Const.GuildLanguageAttributeValue[GUILD_LANGUAGE_ATTRIBUTE_VALUE_SPANISH] = "spanish"
GuildPlannerPro_Const.GuildLanguageAttributeValue[GUILD_LANGUAGE_ATTRIBUTE_VALUE_CHINESE_S] = "chinese"

GuildPlannerPro_Const.RewardType = {}
GuildPlannerPro_Const.RewardType[REWARD_TYPE_ALLIANCE_POINTS] = "alliance_points"
GuildPlannerPro_Const.RewardType[REWARD_TYPE_ARCHIVAL_FORTUNES] = "archival_fortunes"
GuildPlannerPro_Const.RewardType[REWARD_TYPE_AUTO_ITEM] = "auto_item"
GuildPlannerPro_Const.RewardType[REWARD_TYPE_CHAOTIC_CREATIA] = "chaotic_creatia"
GuildPlannerPro_Const.RewardType[REWARD_TYPE_EVENT_TICKETS] = "event_tickets"
GuildPlannerPro_Const.RewardType[REWARD_TYPE_IMPERIAL_FRAGMENTS] = "imperial_fragments"
GuildPlannerPro_Const.RewardType[REWARD_TYPE_INSPIRATION] = "inspiration"
GuildPlannerPro_Const.RewardType[REWARD_TYPE_MONEY] = "money"
GuildPlannerPro_Const.RewardType[REWARD_TYPE_NONE] = "none"
GuildPlannerPro_Const.RewardType[REWARD_TYPE_PARTIAL_SKILL_POINTS] = "partial_skill_points"
GuildPlannerPro_Const.RewardType[REWARD_TYPE_SKILL_LINE] = "skill_line"
GuildPlannerPro_Const.RewardType[REWARD_TYPE_STYLE_STONES] = "style_stones"
GuildPlannerPro_Const.RewardType[REWARD_TYPE_TELVAR_STONES] = "telvar_stones"
GuildPlannerPro_Const.RewardType[REWARD_TYPE_TRIBUTE_CLUB_EXPERIENCE] = "tribute_club_experience"
GuildPlannerPro_Const.RewardType[REWARD_TYPE_UNDAUNTED_KEYS] = "undaunted_keys"
GuildPlannerPro_Const.RewardType[REWARD_TYPE_WRIT_VOUCHERS] = "writ_vouchers"

GuildPlannerPro_Const.HeraldryBackgrounds = {}
local numCategories = GetNumHeraldryBackgroundCategories()
for categoryIndex = 1, numCategories do
    local categoryName, categoryIcon = GetHeraldryBackgroundCategoryInfo(categoryIndex)
    GuildPlannerPro_Const.HeraldryBackgrounds[categoryIndex] = {}

    local numStyles = GetNumHeraldryBackgroundStyles(categoryIndex)
    for styleIndex = 1, numStyles do
        local styleName, styleIcon = GetHeraldryBackgroundStyleInfo(categoryIndex, styleIndex)
        GuildPlannerPro_Const.HeraldryBackgrounds[categoryIndex][styleIndex] = {
            CategoryIndex = categoryIndex,
            CategoryName = categoryName,
            CategoryIcon = categoryIcon,
            StyleIndex = styleIndex,
            StyleName = styleName,
            StyleIcon = styleIcon,
            GuildFinderStyleIcon = GetHeraldryGuildFinderBackgroundStyleIcon(categoryIndex, styleIndex)
        }
    end
end

GuildPlannerPro_Const.HeraldryColors = {}
local numColors = GetNumHeraldryColors()
for colorIndex = 1, numColors do
    local colorName, hueCategory, r, g, b, sortKey = GetHeraldryColorInfo(colorIndex)
    GuildPlannerPro_Const.HeraldryColors[colorIndex] = {
        Index = colorIndex,
        Name = colorName,
        HueCategory = GuildPlannerPro_Const.DyeHueCategory[hueCategory],
        Red = r,
        Green = g,
        Blue = b,
        SortKey = sortKey,
    }
end

GuildPlannerPro_Const.HeraldryCrests = {}
local numCrestCategories = GetNumHeraldryCrestCategories()
for categoryIndex = 1, numCrestCategories do
    local categoryName, categoryIcon = GetHeraldryCrestCategoryInfo(categoryIndex)
    GuildPlannerPro_Const.HeraldryCrests[categoryIndex] = {}

    local numStyles = GetNumHeraldryCrestStyles(categoryIndex)
    for styleIndex = 1, numStyles do
        local styleName, styleIcon = GetHeraldryCrestStyleInfo(categoryIndex, styleIndex)
        GuildPlannerPro_Const.HeraldryCrests[categoryIndex][styleIndex] = {
            CategoryIndex = categoryIndex,
            CategoryName = categoryName,
            CategoryIcon = categoryIcon,
            StyleIndex = styleIndex,
            StyleName = styleName,
            StyleIcon = styleIcon,
            GuildFinderStyleIcon = GetHeraldryGuildFinderCrestStyleIcon(categoryIndex, styleIndex)
        }
    end
end

GuildPlannerPro_Const.MundusAbilityIdMap = {
    [13979] = MUNDUS_STONE_APPRENTICE,
    [13982] = MUNDUS_STONE_ATRONACH,
    [13976] = MUNDUS_STONE_LADY,
    [13978] = MUNDUS_STONE_LORD,
    [13981] = MUNDUS_STONE_LOVER,
    [13943] = MUNDUS_STONE_MAGE,
    [13980] = MUNDUS_STONE_RITUAL,
    [13974] = MUNDUS_STONE_SERPENT,
    [13984] = MUNDUS_STONE_SHADOW,
    [13977] = MUNDUS_STONE_STEED,
    [13975] = MUNDUS_STONE_THIEF,
    [13985] = MUNDUS_STONE_TOWER,
    [13940] = MUNDUS_STONE_WARRIOR,
}

GuildPlannerPro_Const.MidgameContent = {
    "Vateshran Hollows",
    "Blackrose Prison",
    "Maelstrom Arena",
    "Imperial City Prison",
    "White Gold Tower",
}

GuildPlannerPro_Const.EndgameContent = {
    "Lucent Citadel",
    "Sanity's Edge",
    "Dreadsail Reef",
    "Rockgrove",
    "Kyne's Aegis",
    "Sunspire",
    "Cloudrest",
    "Halls of Fabrication",
    "Asylum Sanctorium",
    "Maw of Lorkhaj",
    "Trials",
}
