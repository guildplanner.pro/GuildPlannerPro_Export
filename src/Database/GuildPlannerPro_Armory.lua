GuildPlannerPro_Armory = {}

function GuildPlannerPro_Armory:ExportArmoryBuilds()
    local builds = {}
    local numBuilds = GetNumUnlockedArmoryBuilds()
    for i = 1, numBuilds do
        builds[i] = GuildPlannerPro_Armory:ParseArmoryBuild(i)
    end

    return builds
end

function GuildPlannerPro_Armory:ParseArmoryBuild(buildIndex)
    return {
        BuildIndex = buildIndex,
        Name = GetArmoryBuildName(buildIndex),
        IconIndex = GetArmoryBuildIconIndex(buildIndex),
        AttributePointsSpent = GuildPlannerPro_Armory:GetAttributeSpentPoints(buildIndex),
        ChampionPointsSpent = GuildPlannerPro_Armory:GetChampionSpentPoints(buildIndex),
        Champion = GuildPlannerPro_Skills:ExportChampionPoints(),
        SkillPointsSpent = GetArmoryBuildSkillsTotalSpentPoints(buildIndex),
        SkillLines = GuildPlannerPro_Skills:ExportCharacterSkillLines(),
        Hotbars = GuildPlannerPro_Skills:ExportCharacterHotBars(buildIndex),
        Curse = GetArmoryBuildCurseType(buildIndex),
        Equipment = GuildPlannerPro_Armory:GetEquipment(buildIndex),
        PrimaryMundusStone = GetArmoryBuildPrimaryMundusStone(buildIndex),
        SecondaryMundusStone = GetArmoryBuildSecondaryMundusStone(buildIndex),
        TimeStamp = GetTimeStamp(),
    }
end

function GuildPlannerPro_Armory:GetAttributeSpentPoints(buildIndex)
    local attributeSpentPoints = {}
    for j = ATTRIBUTE_HEALTH, ATTRIBUTE_ITERATION_END do
        table.insert(attributeSpentPoints, {
            Attribute = j,
            PointsSpent = GetArmoryBuildAttributeSpentPoints(buildIndex, j),
        })
    end

    return attributeSpentPoints
end

function GuildPlannerPro_Armory:GetChampionSpentPoints(buildIndex)
    local championSpentPoints = {}
    for k = CHAMPION_DISCIPLINE_TYPE_ITERATION_BEGIN, CHAMPION_DISCIPLINE_TYPE_ITERATION_END do
        table.insert(championSpentPoints, {
            ChampionDisciplineType = k,
            PointsSpent = GetArmoryBuildChampionSpentPointsByDiscipline(buildIndex, k),
        })
    end

    return championSpentPoints
end

function GuildPlannerPro_Armory:GetEquipment(buildIndex)
    local EQUIP_SLOTS =
    {
        EQUIP_SLOT_HEAD,
        EQUIP_SLOT_SHOULDERS,
        EQUIP_SLOT_CHEST,
        EQUIP_SLOT_HAND,
        EQUIP_SLOT_WAIST,
        EQUIP_SLOT_LEGS,
        EQUIP_SLOT_FEET,
        EQUIP_SLOT_NECK,
        EQUIP_SLOT_RING1,
        EQUIP_SLOT_RING2,
        EQUIP_SLOT_MAIN_HAND,
        EQUIP_SLOT_OFF_HAND,
        EQUIP_SLOT_POISON,
        EQUIP_SLOT_BACKUP_MAIN,
        EQUIP_SLOT_BACKUP_OFF,
        EQUIP_SLOT_BACKUP_POISON,
    }
    local equipment = {}
    for _, equipSlot in ipairs(EQUIP_SLOTS) do
        local slotState, bag, slotIndex = GetArmoryBuildEquipSlotInfo(buildIndex, equipSlot)
        if slotState == ARMORY_BUILD_EQUIP_SLOT_STATE_VALID then
            local itemLink = GetItemLink(bag, slotIndex, LINK_STYLE_BRACKETS)
            local _, _, _, _, _, setId = GetItemLinkSetInfo(itemLink, false)
            local traitType, traitDescription = GetItemLinkTraitInfo(itemLink)
            local _, enchantHeader, enchantDescription = GetItemLinkEnchantInfo(itemLink)
            equipment[equipSlot] = {
                ItemId = GetItemLinkItemId(itemLink),
                ItemName = zo_strformat(GetString(SI_TOOLTIP_ITEM_NAME), GetItemName(bag, slotIndex)),
                ItemDisplayQuality = GetItemDisplayQuality(bag, slotIndex),
                ItemLevel = GetItemLevel(bag, slotIndex),
                ItemRequiredLevel = GetItemRequiredLevel(bag, slotIndex),
                ItemRequiredCp = GetItemLinkRequiredChampionPoints(itemLink),
                ItemTrait = traitType,
                ItemTraitDescription = traitDescription,
                ItemTraitCategory = GetItemTraitCategory(bag, slotIndex),
                ItemTraitInformation = GetItemTraitInformation(bag, slotIndex),
                ItemEnchantType = GetEnchantSearchCategoryType(GetItemLinkFinalEnchantId(itemLink)),
                ItemEnchantHeader = enchantHeader,
                ItemEnchantDescription = enchantDescription,
                ItemGlyphMinLevels = GetItemGlyphMinLevels(bag, slotIndex),
                ItemIcon = GetItemLinkIcon(itemLink),
                ItemLink = itemLink,
                SetId = setId,
                EquipSlot = equipSlot,
                EquipType = GetItemLinkEquipType(itemLink),
                ArmorType = GetItemLinkArmorType(itemLink),
                WeaponType = GetItemLinkWeaponType(itemLink),
                ArmorRating = GetItemLinkArmorRating(itemLink, false),
                WeaponPower = GetItemLinkWeaponPower(itemLink),
                TimeStamp = GetTimeStamp(),
            }
        end
    end

    return equipment
end
