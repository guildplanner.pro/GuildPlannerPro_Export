GuildPlannerPro_Collectibles = {}

function GuildPlannerPro_Collectibles:GetCategoryMap()
    local categories = {}
    local numCategories = GetNumCollectibleCategories()
    for categoryId = 1, numCategories do
        local ctgName, ctgNumSubCategories, ctgNumCollectibles, ctgUnlockedCollectibles, ctgTotalCollectibles, hidesLocked = GetCollectibleCategoryInfo(categoryId)
        categories[categoryId] = {
            Id = categoryId,
            Name = ctgName,
            NumSubCategories = ctgNumSubCategories,
            NumCollectibles = ctgNumCollectibles,
            UnlockedCollectibles = ctgUnlockedCollectibles,
            TotalCollectibles = ctgTotalCollectibles,
            HidesLocked = hidesLocked,
            SubCategories = {},
        }

        for subCategoryId = 1, ctgNumSubCategories do
            local subCtgName, subCtgNumCollectibles, subCtgUnlockedCollectibles, subCtgTotalCollectibles = GetCollectibleSubCategoryInfo(categoryId, subCategoryId)
            categories[categoryId].SubCategories[subCategoryId] = {
                Id = subCategoryId,
                Name = subCtgName,
                NumCollectibles = subCtgNumCollectibles,
                UnlockedCollectibles = subCtgUnlockedCollectibles,
                TotalCollectibles = subCtgTotalCollectibles,
            }
        end
    end

    return categories
end

function GuildPlannerPro_Collectibles:MapStoriesCollectibles()
    local collectibleMap = {}
    local categories = self:GetCategoryMap()
    local storiesCategory = categories[1]
    for subCategoryId = 1, storiesCategory.NumSubCategories do
        local subCtgName = storiesCategory.SubCategories[subCategoryId].Name
        collectibleMap[subCtgName] = {}
        for collectibleIndex = 1, storiesCategory.SubCategories[subCategoryId].NumCollectibles do
            local collectibleId = GetCollectibleId(1, subCategoryId, collectibleIndex)
            local name, description, icon, _, _, purchasable, isActive, categoryType, _ = GetCollectibleInfo(collectibleId)
            collectibleMap[subCtgName][collectibleIndex] = {
                Id = collectibleId,
                Name = name,
                Description = description,
                DoesESOPlusUnlockCollectible = DoesESOPlusUnlockCollectible(collectibleId),
                Purchasable = purchasable,
                IsActive = isActive,
                CategoryType = categoryType,
                Icon = icon,
                KeyboardBackgroundImage = GetCollectibleKeyboardBackgroundImage(collectibleId),
                GamepadBackgroundImage = GetCollectibleGamepadBackgroundImage(collectibleId),
                Link = GetCollectibleLink(collectibleId, LINK_STYLE_BRACKETS),
            }
        end
    end

    return collectibleMap
end

function GuildPlannerPro_Collectibles:ExportStoriesCollectibles()
    local collectibleMap = {}
    local categories = self:GetCategoryMap()
    local storiesCategory = categories[1]
    for subCategoryId = 1, storiesCategory.NumSubCategories do
        for collectibleIndex = 1, storiesCategory.SubCategories[subCategoryId].NumCollectibles do
            local collectibleId = GetCollectibleId(1, subCategoryId, collectibleIndex)
            collectibleMap[subCategoryId .. '-' .. collectibleIndex] = {
                Id = collectibleId,
                UnlockState = GetCollectibleUnlockStateById(collectibleId),
            }
        end
    end

    return collectibleMap
end
