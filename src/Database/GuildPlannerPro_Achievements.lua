GuildPlannerPro_Achievements = {
    earlyGameAchievementsTopLevelIndex = 5,
    midGameVeteranAchievementsSubCategoryName = "Veteran",
}

function GuildPlannerPro_Achievements:ExportAchievementCompletionData(filterPersistenceLevel)
    local pveAchievements = self:MapPveAchievements{}
    local achievementCompletion = {}
    for achievementId, achievement in pairs(pveAchievements) do
        if (achievement["PersistenceLevel"] == filterPersistenceLevel) then
            GuildPlannerPro_Achievements:ParseAchievement(achievementId, achievementCompletion)
        end
    end

    return achievementCompletion
end

function GuildPlannerPro_Achievements:GetAchievementsInLine(achievementId)
    local list = {}
    local lineAchievementId = achievementId
    while lineAchievementId ~= 0 do
        lineAchievementId = GetPreviousAchievementInLine(lineAchievementId)
        if lineAchievementId ~= 0 then
            table.insert(list, lineAchievementId)
        end
    end

    return list
end

function GuildPlannerPro_Achievements:ParseAchievement(achievementId, destinationTable)
    -- Get previous achievements in line
    local lineIds = GuildPlannerPro_Achievements:GetAchievementsInLine(achievementId)
    if #lineIds > 0 then
        for _, lineAchievementId in pairs(lineIds) do
            GuildPlannerPro_Achievements:ParseAchievement(lineAchievementId, destinationTable)
        end
    else
        lineIds = nil
    end

    local _, _, _, _, completed, _, _ = GetAchievementInfo(achievementId)
    local criteria, numCriteria = {}, GetAchievementNumCriteria(achievementId)
    if numCriteria then
        for criterionIndex = 1, numCriteria do
            local _, numCompleted, _ = GetAchievementCriterion(achievementId, criterionIndex)
            if numCompleted > 0 then
                table.insert(criteria, {
                    Index = criterionIndex,
                    NumCompleted = numCompleted,
                })
            end
        end
    end

    -- Record it if there is total or partial completion of the achievement
    if completed or #criteria > 0 then
        local characterIdForCompletedAchievement = GetCharIdForCompletedAchievement(achievementId)
        local attributedCharacterNameForCompletedAchievement
        if characterIdForCompletedAchievement then
            attributedCharacterNameForCompletedAchievement = zo_strformat(GetString(SI_PLAYER_NAME), GetCharacterNameById(characterIdForCompletedAchievement))
        end
        destinationTable[achievementId] = {
            AchievementId = achievementId,
            AttributedCharacterName = attributedCharacterNameForCompletedAchievement, -- TODO deprecated
            AttributedCharacterId = Id64ToString(characterIdForCompletedAchievement),
            CompletedAt = GetAchievementTimestamp(achievementId),
            Criteria = criteria,
            LineIds = lineIds,
        }
    end
end

function GuildPlannerPro_Achievements:MapPveAchievements(params)
    setmetatable(params, { __index = { shallow = false } })
    local shallow = params[1] or params.includeOnlyAccountData

    local pveAchievements = {}
    local numCategories = GetNumAchievementCategories()
    for topLevelIndex = 1, numCategories do
        -- Skip early-game content achievements.
        if (topLevelIndex ~= self.earlyGameAchievementsTopLevelIndex) then
            local topLevelName, numSubCategories = GetAchievementCategoryInfo(topLevelIndex)
            if numSubCategories > 0 then
                for subCategoryIndex = 1, numSubCategories do
                    local subCategoryName, subNumAchievements = GetAchievementSubCategoryInfo(topLevelIndex, subCategoryIndex)
                    if subNumAchievements > 0 then
                        for achievementIndex = 1, subNumAchievements do
                            local achievementId = GetAchievementId(topLevelIndex, subCategoryIndex, achievementIndex)
                            local achievementName, achievementDescription, achievementPoints, achievementIcon = GetAchievementInfo(achievementId)
                            if (
                                    subCategoryName == self.midGameVeteranAchievementsSubCategoryName
                                            or GuildPlannerPro_Utils:InTable(subCategoryName, GuildPlannerPro_Const.MidgameContent)
                                            or GuildPlannerPro_Utils:InTable(subCategoryName, GuildPlannerPro_Const.EndgameContent)
                            ) then
                                pveAchievements[achievementId] = {}
                                pveAchievements[achievementId]["PersistenceLevel"] = GetAchievementPersistenceLevel(achievementId)
                                if not shallow then
                                    pveAchievements[achievementId]["Id"] = achievementId
                                    pveAchievements[achievementId]["TopLevelIndex"] = topLevelIndex
                                    pveAchievements[achievementId]["TopLevelName"] = topLevelName
                                    pveAchievements[achievementId]["SubCategoryIndex"] = subCategoryIndex
                                    pveAchievements[achievementId]["SubCategoryName"] = subCategoryName
                                    pveAchievements[achievementId]["Index"] = achievementIndex
                                    pveAchievements[achievementId]["FullPath"] = zo_strformat("<<1>>/<<2>>/<<3>>", topLevelName, subCategoryName, achievementName)
                                    pveAchievements[achievementId]["Name"] = achievementName
                                    pveAchievements[achievementId]["Description"] = achievementDescription
                                    pveAchievements[achievementId]["Points"] = achievementPoints
                                    pveAchievements[achievementId]["Icon"] = achievementIcon
                                    pveAchievements[achievementId]["LineIds"] = GuildPlannerPro_Achievements:GetAchievementsInLine(achievementId)

                                    if (subCategoryName == self.midGameVeteranAchievementsSubCategoryName or GuildPlannerPro_Utils:InTable(subCategoryName, GuildPlannerPro_Const.MidgameContent)) then
                                        pveAchievements[achievementId]["ContentCategory"] = "mid_game"
                                    else
                                        pveAchievements[achievementId]["ContentCategory"] = "end_game"
                                    end

                                    pveAchievements[achievementId]["Criteria"] = {}
                                    local numCriteria = GetAchievementNumCriteria(achievementId)
                                    if numCriteria then
                                        for criterionIndex = 1, numCriteria do
                                            local description, _, numRequired = GetAchievementCriterion(achievementId, criterionIndex)
                                            pveAchievements[achievementId]["Criteria"][criterionIndex] = {
                                                Index = criterionIndex,
                                                Description = description,
                                                NumRequired = numRequired,
                                            }
                                        end
                                    end

                                    pveAchievements[achievementId]["Rewards"] = {}
                                    local hasRewardItem, itemName, itemIcon, displayQuality = GetAchievementRewardItem(achievementId)
                                    if hasRewardItem then
                                        table.insert(pveAchievements[achievementId]["Rewards"], {
                                            Type = "Item",
                                            Name = itemName,
                                            Icon = itemIcon,
                                            DisplayQuality = displayQuality,
                                        })
                                    end
                                    local hasRewardTitle, title = GetAchievementRewardTitle(achievementId)
                                    if hasRewardTitle then
                                        table.insert(pveAchievements[achievementId]["Rewards"], {
                                            Type = "Title",
                                            Title = title,
                                        })
                                    end
                                    local hasRewardDye, dyeId = GetAchievementRewardDye(achievementId)
                                    if hasRewardDye then
                                        local dyeName, _, dyeRarity, _, _, r, g, b = GetDyeInfoById(dyeId)
                                        table.insert(pveAchievements[achievementId]["Rewards"], {
                                            Type = "Dye",
                                            DyeInfo = {
                                                Name = dyeName,
                                                Rarity = dyeRarity,
                                                Red = r,
                                                Green = g,
                                                Blue = b,
                                            }
                                        })
                                    end
                                    local hasRewardCollectible, collectibleId = GetAchievementRewardCollectible(achievementId)
                                    if hasRewardCollectible then
                                        local name, description, icon, _, _, _, _, categoryType, hint = GetCollectibleInfo(collectibleId)
                                        table.insert(pveAchievements[achievementId]["Rewards"], {
                                            Type = "Collectible",
                                            Collectible = {
                                                Id = collectibleId,
                                                Name = name,
                                                Description = description,
                                                Icon = icon,
                                                CategoryType = categoryType,
                                                IconHint = hint,
                                            },
                                        })
                                    end
                                    local hasRewardCardUpg, tributePatronId, tributeCardIndex = GetAchievementRewardTributeCardUpgradeInfo(achievementId)
                                    if hasRewardCardUpg then
                                        table.insert(pveAchievements[achievementId]["Rewards"], {
                                            Type = "TributeCardUpgrade",
                                            TributePatron = {
                                                Id = tributePatronId,
                                                Icon = {
                                                    Small = GetTributePatronSmallIcon(tributePatronId),
                                                    Large = GetTributePatronLargeIcon(tributePatronId),
                                                    LargeRing = GetTributePatronLargeRingIcon(tributePatronId),
                                                    Suit = GetTributePatronSuitIcon(tributePatronId),
                                                }
                                            },
                                            TributeCard = {
                                                Index = tributeCardIndex,
                                            },
                                        })
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
    end

    return pveAchievements
end
